### UI Tests

#### THIS PROJECT
mimic dependencies as in the panera app
running gradle sdk 11

#### QUICK SUMMARY
using espresso + managed gradle devices for ui testing

managed gradle devices benefits
- gradle spins up the simulator for us
- has test sharding support (can split tests between multiple emulators concurrently to improve test time / save build minutes)

#### STARTING INSTRUMENTED TESTS

##### for this app
`./gradlew pixel2api30Debug`

##### for panera qac
` ./gradlew pixel2api30QuickQarc`