package com.example.uitestingexample

import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.hasText
import androidx.compose.ui.test.junit4.ComposeContentTestRule
import androidx.compose.ui.test.junit4.createComposeRule
import androidx.compose.ui.test.onAllNodesWithText
import androidx.compose.ui.test.onNodeWithText
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.uitestingexample.ui.theme.UitestingexampleTheme

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.junit.Rule

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    private fun ComposeContentTestRule.waitForText(string: String) {
        setContent {
            MainScreen()
        }

        waitUntil(timeoutMillis = 60000) {
            composeTestRule
                .onAllNodesWithText(string)
                .fetchSemanticsNodes().size == 1
        }

        onNodeWithText(string).assertIsDisplayed()
    }

    @Test
    fun Test_One() {
        composeTestRule.waitForText("Hello Android!")
    }

    @Test
    fun Test_Two() {
        composeTestRule.waitForText("Test 2")
    }

    @Test
    fun Test_Three() {
        composeTestRule.waitForText("Test 3")
    }
}